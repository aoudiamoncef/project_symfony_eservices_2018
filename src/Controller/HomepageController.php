<?php
/**
 * Created by PhpStorm.
 * User: aoudia
 * Date: 29/11/17
 * Time: 08:52
 */

namespace App\Controller;

use App\Entity\Tournament;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomepageController extends AbstractController {


    /**
     * @Route("/homepage", name="homepage")
     */
    public function index(Request $request):Response{


        $tournaments = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(Tournament::class)
            ->findAll();


//        $tournaments = [
//            ['name'=> 'Tournament 1'],
//            ['name'=> 'Tournament 2'],
//            ['name'=> 'Tournament 3'],
//            ['name'=> 'Tournament 4'],
//            ['name'=> 'Tournament 5'],
//            ['name'=> 'Tournament 6'],
//            ['name'=> '<script>alert("Hello world")</script>']
//        ];

        return $this->render("homepage.html.twig",
            [
                'tournaments' => $tournaments,
                'message' => $request->query->get('message','pas de message'),
            ]);
    }
}